package com.ciber.devops_news

import java.io.PrintStream;
import java.io.File;
import java.time.LocalDate;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;

// this is not working, not sure how to include the scala libraries on the class path
// cd /Users/markhahn/Documents/code/spring-custom/DevOpsNewsScala
// scala -cp build/libs/DevOpsNewsScala.jar com.ciber.devops_news.CreateDevOpsNewsPage
// ? The -cp option seems to 

// Alternate, uses the scalac command, and then run
// scalac src/main/scala/com/ciber/devops_news/CreateDevOpsNewsPage.scala 
// scala com.ciber.devops_news.CreateDevOpsNewsPage

object CreateDevOpsNewsPage {

  val searchStrings = Array(
    "devops", "cloud computing", "aws azure gcp", "devops market", "cloud computing market")

  val newsSites = Array(
    "cnet.com", "computerworld.com", "infoworld.com", "itworld.com", "informationweek.com",
    "techtarget.com", "eweek.com", "techcrunch.com", "technologyreview.com", "geekwire.com",
    "theregister.co.uk", "gartner.com", "idg.com")

  val searchEngines = Array("https://www.google.com/search?q=")

  val searches = Map(
    "News Past Week" -> "&tbm=nws&tbs=qdr:w",
    "News Past Month" -> "&tbm=nws&tbs=qdr:m")

  
  def main(args: Array[String]) = {

    def buildOutputFileName(): File = {
      var home = sys.env("HOME")
      println(home)
      var filename = "x";
      return new File(home, "Documents/news_searches.html");
    }

    def buildOutputPrintStream(f: File): PrintStream = {
      println(f.getAbsolutePath)
      return new PrintStream(f)
    }

    def buildTopic(title: String, searchOptions: String, utm: String, out: PrintStream) = {
      printf("\n%s\n", title)
      out.printf("<h2>%s</h2>\n", title)
      for (searchTerm <- searchStrings) {
        for (newsSite <- newsSites) buildTermSearch(searchTerm, newsSite)
        out.printf("<br/>\n");
      }

      def buildTermSearch(term: String, site: String) = {
        for (engine <- searchEngines) href(engine, term.replace(" ", "+"));

        def href(engine: String, searchString: String) = {
          var href = engine + searchString + "+site:" + site + searchOptions + utm
          println(href)

          out.printf("  <a href='%s'>%s %s</a>\n", href, site, term);
          out.printf("<br/>\n");
        }
      }
    }

    println("Hello Scala !!")

    var f = buildOutputFileName

    // the LocalDate.with function clashes with Scala reserved word with
    // the backticks quote it away from the Scala parser
    var utm = "&utm=" + LocalDate.now().`with`(DayOfWeek.MONDAY)

    var ps = buildOutputPrintStream(f)

    ps.printf("<html>\n");
    ps.printf("<title>DevOps News Searches</title>\n");
    ps.printf("<body>\n");

    for ((title, searchOptions) <- searches) buildTopic(title, searchOptions, utm, ps)

    ps.printf("</body></html>\n");
    ps.flush()
    ps.close()

    println(f.getAbsolutePath)
    println("Done at " + LocalDate.now().format(DateTimeFormatter.ISO_DATE))
  }
}
